<!-- получаем из json со строчными данными массив,
преобрназовываем в переменную:
обязательно параметр тру чтобы не преобразовало в обьект! -->
    <?php
    $CookiePhpSet = 0;
        if  (!empty($_COOKIE['CookieJson']))
      { $CookiePhpSet = json_decode ($_COOKIE['CookieJson'], true);  };
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Homework hillel #10 | COOKIE PHP">
  <title>HW10 MOROZ COOKIES PHP</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>


<div class ="container">
    <div class="row d-flex justify-content-center">
        <div class="col-8  display-8">
        <H1>Hillel Student Moroz HW#9 OOP PHP</H1>
        </div>
    </div>
</div>
      <div class = "container-fluid">
<!-- Добавили форму с текстовым полем  -->
            <form  class="row g-3 m-3"
                  action="addTask.php"
                  method="post">
                  
              <div class="col-auto">
                  <label><h3>Task Text</h3></label>
<!-- Отправляем постом name = "information" и кнопка отправить и все  -->
                  <input type="text" 
                  class="form-control-plaintext"
                  value=" Input Task Message There"
                  name="information">
              </div>
              <div>
              <button
                  type="submit"
                  class="btn btn-outline-info mb-3"
                  name="taskSend">
                  Send
              </button>
          </form> 
      </div>
</form>
</div>
<!-- Конец формы ввода текста -->



      <div class = "container-fluid">
<!-- Шапка таблички с задачами // ключ массива /
/ текст задачи / зачеркнуто / выполнено или нет / кнопка выполнено -->
      <table class="table align-middle table-bordered">
          <tr>
                <th>Number</th>
                <th>Text</th>
                <th>Completed</th>
                <th>Button</th>
          </tr>


        <?php
    // формируем цикл с выводом строк таблички
    if (!empty($CookiePhpSet)):
    foreach ($CookiePhpSet as $key => $CookiePhp):
        ?>

  <tr>
	    <td><?=$key;?></td>
<!-- тут формируем теги для зачеркивания  задач -->
      <td><?php if ($CookiePhp['completed'] === 'true'):echo '<strike>';  endif;//del ?>
<!-- тут выводится текст любой задачи -->
      <?=$CookiePhp['title'];?>
<!-- тут закрываем теги вызванные для зачеркивания задач -->
      <?php if ($CookiePhp['completed'] === 'true'):echo '</strike>';  endif; //del ?></td>
<!-- тут изменяемое поле 'Comlpeted' -->
      <td><?=(string)$CookiePhp['completed'];?></td>
<!-- тут ссылка для скрипта выполнить -->
      <td><a href="/completeTask.php?key=<?=(string)$key?>">Complete</a></td>
      </td>
  </tr>
       <?php endforeach; // закрываем цикл и вывод на экран 
       endif;?>
      </table>
        </div>
    </div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
</html>